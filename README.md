
# Hooper

Vue.js carousel component, optimized to work with Vue. (Based on [Hooper(Baianat)](https://baianat.github.io/hooper/))

## Features

* Easily customizable through rich API and addons.
* Touch, Keyboard, Mouse Wheel, and Navigation support.
* Two way control carousels (sync).
* Full RTL layout support.
* Supports vertical sliding.
* Responsive breakpoints.
* Seamless infinite scroll.
* Accessible by providing a robust structure and user control.
* Optimized to work with Vue framework.
* SSR Support.

## Browser Support
| ![Chrome](https://raw.github.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png) | ![Firefox](https://raw.github.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png) | ![Safari](https://raw.github.com/alrra/browser-logos/master/src/safari/safari_48x48.png) | ![Opera](https://raw.github.com/alrra/browser-logos/master/src/opera/opera_48x48.png) | ![Edge](https://raw.github.com/alrra/browser-logos/master/src/edge/edge_48x48.png) | ![IE](https://raw.github.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_48x48.png) |
| --- | --- | --- | --- | --- | --- |
| Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ | Latest ✔ | 11 ✔ |

## Getting started

### Installation

First step is to install it using `yarn` or `npm`:

```bash
npm install hooper

# or use yarn
yarn add hooper
```


### Deployemnt for `npm install hooper`

First step is to delete all files in dist folder and push deleted files. (This is because of preventing conflicts of builded dist files later)

Bulld src files after it, it will re-generate dist files. And dist files are used in node module <hooper> when you installed hooper on [prototype](https://bitbucket.org/accommodationuk/prototype) repository

```bash
npm run build

# or
node scripts/build
```


### Use Hooper

```vue
<template>
  <hooper>
    <slide>
      slide 1
    </slide>
    <slide>
      slide 2
    </slide>
    ...
  </hooper>
</template>

<script>
  import { Hooper, Slide } from 'hooper';
  import 'hooper/dist/hooper.css';

  export default {
    name: 'App',
    components: {
      Hooper,
      Slide
    }
  }
</script>
```

> If you are using [PurgeCSS](https://www.purgecss.com/), make sure to whitelist hooper css When importing `hooper/dist/hooper.css`.

more info at [Documentation](https://baianat.github.io/hooper/)

### Available Props

|Prop             |Default |Description|
|-----------------|-----|-----------|
|`itemsToShow`    |1    |count of items to showed per view  (can be a fraction).|
|`itemsToSlide`   |1    |count of items to slide when use navigation buttons.|
|`initialSlide`   |0    |index number of initial slide.|
|`infiniteScroll` |false|enable infinite scrolling mode.|
|`centerMode`     |false|enable center mode.|
|`vertical`       |false|enable vertical sliding mode.|
|`rtl`            |null |enable rtl mode.|
|`mouseDrag`      |true |toggle mouse dragging.|
|`touchDrag`      |true |toggle touch dragging.|
|`wheelControl`   |true |toggle mouse wheel sliding.|
|`keysControl`    |true |toggle keyboard control.|
|`shortDrag`      |true |enable any move to commit a slide.|
|`autoPlay`       |false|enable auto sliding to carousel. This could be changed dynamically.|
|`playSpeed`      |2000 |speed of auto play to trigger slide in ms.|
|`transition`     |300  |sliding transition time in ms.|
|`sync`           |''   |sync two carousels to slide together.|
|`hoverPause`     |true |pause autoPlay if the mouse enters the slide.|
|`trimWhiteSpace` |false|limit carousel to slide only when there will be no completely empty slide-space.|
|`settings`       |{ }  |an object to pass all settings.|
|                 |     | |
|New added prop   |     | |
|                 |     | |
|`animate`        |false|enable animation when switch to another slide when it is `true`. This will add class `anim-fadein` and `anim-fadeout` when sliding, and these classes will be removed when sliding is finshed|
|`scroll`         |false|show the scroll indicator on 1st slide|

### Available CSS Properties

Please also look at the source to style the slider. An initial style can be imported as
`@import '~hooper/dist/hooper.css';`

|Class            | Property | Default | Description|
|-----------------|----------|---------|------------|
|`hooper`         | `height` | 200px   | the default height of the slider, set to auto to scale with content
